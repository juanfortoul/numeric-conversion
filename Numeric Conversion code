import java.util.Scanner;

/* Juan Fortoul
*  Programming Fundamentals 1
*  10/02/19
*  Lab 04 */

public class NumericConversion {

    public static long hexStringDecode(String hex) {  //Create new method to decode hexadecimals
        long result = 0;           //The result of the decoding must be a long variable to match the method data type
        char charHex;              //This character variable will replace hex.charAt(i) when necessary
        for(int i = 0; i < hex.length(); ++i) { //For loop is created to create a summation operator
            charHex = hex.charAt(i);
            if (hex.substring(0, 2).equals("0x")) { //If statement to account for the hexadecimal prefix "0x"
                hex = hex.substring(2, hex.length()); //The hexadecimal has the prefix removed to continue the decoding
                charHex = hex.charAt(i);            //The charHex variable must be updated to match the new hexadecimal
                if (Character.isDigit(charHex)) {   //If statement for when charHex is a digit
                    result += ((hex.charAt(i) - '0') * Math.pow(16, hex.length() - (i + 1)));
                    /* The decoding formula is a summation operator in which the decoded hexadecimal is equal to the
                    * character value of a given index multiplied by the base of hexadecimal, 16 to the hex string
                    * length minus 1 plus the index value. this process is repeated until all the characters at each
                    * index is accounted for. */
                } else {  //If statement for if the character is a letter
                    charHex = Character.toUpperCase(charHex);
                    result += (charHex - 'A' + 10) * Math.pow(16, hex.length() - (i + 1));
                }               //The decoding formula is modified to account for letter characters
            } else {
                if (Character.isDigit(charHex)) {         //If statements when the hexadecimal has no prefix
                    result += ((charHex - '0') * Math.pow(16, hex.length() - (i + 1)));
                } else {
                    charHex = Character.toUpperCase(charHex);
                    result += (charHex - 'A' + 10) * Math.pow(16, hex.length() - (i + 1));
                }
            }
        }
        return result; //The result is the result of the summation operator within the for loop which gives the decimal
    }

    public static short hexCharDecode(char digit) { //Create method to decode a hexadecimal with one character
        short result = 0;
        if (Character.isLetter(digit)) {   //If statement for if the character is a letter
            digit = Character.toUpperCase(digit);
            result = (short) ((digit - 'A' + 10) * Math.pow(16, 0));
        } else {
            result = (short) ((digit - '0') * Math.pow(16, 0));
        }
        return result; //The result is merely the value of the character itself, since there is only one character
    }

    public static short binaryStringDecode(String binary) { //Create method to decode a binary string
        short result = 0;
        for(int i = 0; i < binary.length(); ++i) {
            if (binary.substring(0, 2).equals("0b")) { //If statement to account for binary string prefix "0b"
                binary = binary.substring(2, binary.length());
                    result += ((binary.charAt(i) - '0') * Math.pow(2, binary.length() - (i + 1)));
            } else {
                result += ((binary.charAt(i) - '0') * Math.pow(2, binary.length() - (i + 1)));
            }                                                     //The base is changed to 2 since it is a binary string
        }
        return result; //The result is the result of the summation operator within the for loop which gives the decimal
    }

    public static String binaryToHex(String binary) { //Create method to convert a binary to a hexadecimal
        int result = 0; //The result is the result of first decoding the binary string
        int remainder = 1; //The remainder quotient are needed to convert the decoded binary string to a hexadecimal
        int quotient = 1;
        String hex = ""; //Initialize the string variable for the final hexadecimal
        for(int i = 0; i < binary.length(); ++i) { //Create for loop for decoding a binary string
            if (binary.substring(0, 2).equals("0b")) {
                binary = binary.substring(2, binary.length());
                result += ((binary.charAt(binary.length() - 1 - i) - '0') * Math.pow(2, i));
            } else {
                result += ((binary.charAt(binary.length() - 1 - i) - '0') * Math.pow(2, i));
            }
        }
        for (int i = 1; quotient > 0; ++i) { //Create for loop to convert a decoded binary string to a hexadecimal
            quotient = result / 16;
            remainder = result - ((result / 16) * 16);
            /*First the result must be divided by the hexadecimal base of 16. The first remainder is then calculated
            * which then becomes the last character of the converted hexadecimal. If the remainder is between zero and
            * 9, the new character matches the remainder and is added to the back of the hexadecimal. If the remainder
            * is between 10 and 15, the new character can be between A and F respectively. This process continues
            * until the remainder reaches 0. */
            if (remainder >= 0 && remainder <= 9) { //If statement for remainders between 0 and 9
                if (remainder == 0) {
                    hex = "0" + hex;
                } else if (remainder == 1) {
                    hex = "1" + hex;
                } else if (remainder == 2) {
                    hex = "2" + hex;
                } else if (remainder == 3) {
                    hex = "3" + hex;
                } else if (remainder == 4) {
                    hex = "4" + hex;
                } else if (remainder == 5) {
                    hex = "5" + hex;
                } else if (remainder == 6) {
                    hex = "6" + hex;
                } else if (remainder == 7) {
                    hex = "7" + hex;
                } else if (remainder == 8) {
                    hex = "8" + hex; //The hex variable is updated this way so the new character is placed in the back
                } else {
                    hex = "9";
                }
            } else {             //If statement for if the remainder is between 10 and 15
                if (remainder == 10) {
                    hex = "A" + hex;
                } else if (remainder == 11) {
                    hex = "B" + hex;
                } else if (remainder == 12) {
                    hex = "C" + hex;
                } else if (remainder == 13) {
                    hex = "D" + hex;
                } else if (remainder == 14) {
                    hex = "E" + hex;
                } else {
                    hex = "F" + hex;
                }
            }
            result /= 16;  //The result must be updated so the for loop can continue effectively
        }
        return hex;      //Once the for loop is complete the newlt converty hexadecimal is returned to the method call
    }

    public static void main(String[] args) {   //Main method
       // write your code here
        Scanner scnr = new Scanner(System.in); //Create Scanner
        int optionNum = 0;   //Menu option
        String stringNum;    //Numeric String to convert
        char charNum;        //Single character to convert

        while (optionNum != 4) {  //Create while loop to iterate menu options, ends when chosen menu option is 4
            System.out.println("Decoding Menu");          //Output Decoding Menu
            System.out.println("-------------");
            System.out.println("1. Decode hexadecimal");
            System.out.println("2. Decode binary");
            System.out.println("3. Convert binary to hexadecimal");
            System.out.println("4. Quit");
            System.out.println();
            System.out.println("Please enter an option: ");
            optionNum = scnr.nextInt();          //Input menu option
            if (optionNum == 1) {                //If statement for when option 1 is chosen
                System.out.println("Please enter the numeric string to convert: ");
                stringNum = scnr.next();         //Import numeric string to convert
                if (stringNum.length() == 1) {   //If statement for when the string is a single character
                    charNum = stringNum.charAt(0);
                    System.out.println("Result: " + hexCharDecode(charNum)); //Method call for hexCharDecode()
                    System.out.println();
                } else if (stringNum.length() > 1) { //If statement for when the string is more than one character
                    System.out.println("Result: " + hexStringDecode(stringNum)); //Method call for hexStringDecode()
                    System.out.println();
                }
            }
            if (optionNum == 2) {         //If Statement for when option 2 is chsoen
                System.out.println("Please enter the numeric string to convert: ");
                stringNum = scnr.next();
                System.out.println("Result: " + binaryStringDecode(stringNum)); //Method Calls for binaryStringDecode()
                System.out.println();
            }
            if (optionNum == 3) {           //If statement for option 3
                System.out.println("Please enter the numeric string to convert: ");
                stringNum = scnr.next();
                System.out.println("Return: " + binaryToHex(stringNum)); //Method call for binaryToHex
                System.out.println();
            }
            if (optionNum == 4) {      //When option 4 is chosen, the while loop ends
                System.out.println("Goodbye!");
            }
        }
    }
}
